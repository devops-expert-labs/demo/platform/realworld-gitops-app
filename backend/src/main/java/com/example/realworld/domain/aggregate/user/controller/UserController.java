package com.example.realworld.domain.aggregate.user.controller;

import com.example.realworld.domain.aggregate.user.dto.UserAuth;
import com.example.realworld.domain.aggregate.user.dto.UserResponse;
import com.example.realworld.domain.aggregate.user.dto.UserUpdate;
import com.example.realworld.domain.aggregate.user.service.UserService;

import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "User information", description = "User information APIs - Gets the currently logged-in user or Updated user information for current user")
@Slf4j
@RestController
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public UserResponse currentUser(@AuthenticationPrincipal UserAuth userAuth) {
        return userService.getCurrentUser(userAuth);
    }

    @PutMapping
    public UserResponse updateUser(@Valid @RequestBody UserUpdate userUpdate, @AuthenticationPrincipal UserAuth userAuth) {
        return userService.updateUser(userUpdate, userAuth);
    }

}
