package com.example.realworld.domain.service;

import com.example.realworld.security.jwt.JwtConfig;
import com.example.realworld.domain.aggregate.user.dto.UserAuth;
import com.example.realworld.domain.aggregate.user.service.UserServiceDetail;
import com.example.realworld.domain.aggregate.user.entity.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;

@Service
@Slf4j
@Getter
@AllArgsConstructor
public class JwtService {

    private final JwtConfig jwtConfig;

    private final UserServiceDetail userServiceDetail;

    private Key getSignKey(String secretKey) {
        byte[] keyBytes = secretKey.getBytes(StandardCharsets.UTF_8);
        return Keys.hmacShaKeyFor(keyBytes);
    }

    public Claims extractAllClaims(String token) throws ExpiredJwtException {
        return Jwts.parserBuilder()
                .setSigningKey(getSignKey(jwtConfig.getSecretKey()))
                .build()
                .parseClaimsJws(token)
                .getBody();
    }

    public String getEmail(String token) {
        return extractAllClaims(token).get("email", String.class);
    }

    public Boolean isTokenExpired(String token) {
        final Date expiration = extractAllClaims(token).getExpiration();
        return expiration.after(new Date());
    }

    public String createToken(String email) {
        Claims claims = Jwts.claims();
        claims.put("email", email);

        Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(new Date(now.getTime() + jwtConfig.getValidityInMillis()))
                .signWith(getSignKey(jwtConfig.getSecretKey()))
                .compact();
    }

    public Boolean validateToken(String token) {
        return isTokenExpired(token);
    }

    public Authentication getAuthentication(String jwtToken) {
        UserDetails userDetails = userServiceDetail.loadUserByUsername(getEmail(jwtToken));
        User user = (User) userDetails;

        UserAuth authenticatedUser = UserAuth.builder()
                .bio(user.getBio())
                .image(user.getImage())
                .username(user.getUsername())
                .id(user.getId())
                .email(user.getEmail()).build();

        return new UsernamePasswordAuthenticationToken(authenticatedUser, "", userDetails.getAuthorities());
    }

}
