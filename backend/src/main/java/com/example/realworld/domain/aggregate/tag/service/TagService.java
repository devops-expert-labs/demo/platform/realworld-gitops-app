package com.example.realworld.domain.aggregate.tag.service;

import java.util.List;

public interface TagService {
    List<String> getTags();
}
