package com.example.realworld.domain.aggregate.article.repository;

import com.example.realworld.domain.aggregate.article.entity.Article;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    @EntityGraph(attributePaths = "tags")
    @Query("SELECT a FROM Article a INNER JOIN a.tags t WHERE t.name =:tag ORDER BY a.createdAt DESC")
    List<Article> findByTag(@Param("tag") String tag, Pageable pageable);

    @EntityGraph(attributePaths = "tags")
    @Query("SELECT a FROM Article a WHERE a.author.username = :author ORDER BY a.createdAt DESC")
    List<Article> findByAuthorName(String author, Pageable pageable);

    @EntityGraph(attributePaths = "tags")
    @Query("SELECT a FROM Article a LEFT JOIN Favorite f ON f.article.id = a.id WHERE f.user.username =:username ORDER BY a.createdAt DESC")
    List<Article> findByFavoritedUser(String username, Pageable pageable);

    @EntityGraph(attributePaths = "tags")
    @Query("SELECT a FROM Article a ORDER BY a.createdAt DESC")
    List<Article> findByAll(Pageable pageable);
}
