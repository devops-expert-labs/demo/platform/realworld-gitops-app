package com.example.realworld.domain.aggregate.article.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;

import java.time.ZonedDateTime;
import java.util.List;

@Builder
@Getter
public class CommentResponse {
    private Long id;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime createdAt;
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX")
    private ZonedDateTime updatedAt;
    private String body;

    private Author author;

    @Builder
    @Getter
    public static class Author {
        private String username;
        private String bio;
        private String image;
        private Boolean following;
    }

    @Builder
    @Getter
    public static class SingleComment {
        CommentResponse comment;
    }

    @Builder
    @Getter
    public static class MultiComments {
        List<CommentResponse> comments;
    }
}
