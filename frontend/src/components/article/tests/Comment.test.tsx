import React from 'react'
import { render, screen } from '@testing-library/react'
import { RecoilRoot } from 'recoil'

import Comment from '../Comment'
import { mockComment as comment } from './mockData'
import { BrowserRouter } from 'react-router-dom'
import { convertToDate } from '../../../utils'

describe('Comment component', () => {
  const removeComment = jest.fn()

  beforeEach(() => {
    render(
      <BrowserRouter>
        <Comment comment={comment} removeComment={removeComment} />
      </BrowserRouter>,
      { wrapper: RecoilRoot }
    )
  })

  it('renders comment body', () => {
    expect(screen.getByText(comment.body)).toBeInTheDocument()
  })

  it('renders author image and username', () => {
    const authorImg = screen.getByRole('img')
    const authorName = screen.getByRole('link', { name: comment.author.username })
    expect(authorImg).toBeInTheDocument()
    expect(authorName).toBeInTheDocument()
    expect(authorImg).toHaveAttribute('src', comment.author.image)
    expect(authorName).toHaveAttribute('href', `/profile/${comment.author.username}`)
  })

  it('renders formatted creation date', () => {
    expect(screen.getByText(convertToDate(comment.createdAt))).toBeInTheDocument()
  })
})
